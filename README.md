# Youtube-Video-Downloader
A python script based on python pytube library to download videos from youtube

## Install pytube
``` pip install pytube ```

## How to run

* Clone the repository:

``` $ git clone https://hmr_270@bitbucket.org/hmr_270/youtube-video-downloader.git ``` 

* cd into directory into your command prompt: 

```$ cd Youtube-Video-Downloader ```
 
* Run the downloader.py file: 

```$ python downloader.py ```

* If you want to download the playlist then run the playlist_downloader.py: 

```$ python playlist_downloader.py ```


### Check the downloaded file

Note: You can change url in the downloader.py file to the url of the video you want to download.

      After running python downloader.py command your video will be downloaded in the same directory in a few minutes.